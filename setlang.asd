(defsystem #:setlang
    :depends-on (#:alexandria #:split-sequence)
    :components ((:file "setlang")))
