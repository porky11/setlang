(defpackage #:setlang
  (:use #:cl #:alexandria #:split-sequence)
  (:export
   ;;for language parsing
   #:expand-text
   #:call-text
   #:text
   #:expand-file
   #:call-file
   #:file
   #:with-context
   #:defcall

   ;;definition macros
   #:defabsolute
   #:defabsolutes
   #:defrelative
   #:defrelatives
   #:defbinary
   #:defbinaries
   #:defmodifier
   #:defmodifiers
   #:defexpander
   #:defexpanders

   #:*language-package*

   ;;definition helpers
   #:words
   #:form
   #:expand
   #:expand-list
   #:add-absolute
   #:add-relative
   #:add-binary
   #:add-modifier
   #:add-expander
   ))
(in-package #:setlang)

(defvar *words* (make-hash-table))
(defvar *language-package* nil)

(defun word-type (word)
  (or (car (gethash word *words*)) :undefined))

(defun intern-all (word)
  (intern (format nil "~a" word) (or *language-package* *package*)))

(defun absolutep (word)
  (eq (car (gethash word *words*)) :absolute))

(defun relativep (word)
  (eq (car (gethash word *words*)) :relative))

(defun binaryp (word)
  (eq (car (gethash word *words*)) :binary))

(defun modifierp (word)
  (eq (car (gethash word *words*)) :modifier))

(defun expanderp (word)
  (eq (car (gethash word *words*)) :expander))

(defun parse-list (words &optional level single)
  (do ((result)
       (word (car words) (car words))
       (words (cdr words) (cdr words)))
      ((not word) (cons (intern-all "") (reverse result)))
    (when (and single result)
      (return (values (car result) (cons word words))))
    (let ((type (word-type word)))
      (if (eq type :expander)
          (multiple-value-bind (new-result new-words) (expand-expander word words)
            (push (cons (intern-all "") new-result) result)
            (setf words new-words)
            (if level
                (return (values (if single (car result) (cons (intern-all "") (reverse result))) words))
                (setf result (list (cons (intern-all "") (reverse result))))))
          (if (modifierp (car words))
              (push (expand-modifier (pop words) word) words)
              (case type
                (:absolute
                 (push word result)
                 (if level
                     (return (values (if single (car result) (cons (intern-all "") (reverse result))) words))
                     (setf result (list (cons (intern-all "") (reverse result))))))
                (:relative
                 (multiple-value-bind (new-result new-words) (parse-list words t)
                   (push (list word new-result) result)
                   (setf words new-words)))
                (:binary
                 (if result
                     (multiple-value-bind (new-result new-words) (parse-list words t)
                       (push (list word (pop result) new-result) result)
                       (setf words new-words))
                     (error "No set before binary expression ~S" word))
                 (if level
                     (return (values (if single (car result) (cons (intern-all "") (reverse result))) words))
                     (setf result (list (cons (intern-all "") (reverse result))))))
                (:modifier
                 (error "Modifier not allowed here"))
                (t (error "Word ~S does not exist" word))))))))

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun whitespacep (char)
  (member char *whitespaces* :test #'char=))

(defun parse-sentence (string)
  (parse-list (mapcar #'intern-all (delete "" (split-sequence-if #'whitespacep string) :test #'string=))))

(defun parse-text (string)
  (let ((list (mapcar #'parse-sentence
                      (delete "" (split-sequence #\. string) :test #'string=))))
    (cons (intern-all "") list)))

(defun expand (expression)
  (if (listp expression)
      (destructuring-bind (word . words) expression
        (cond
          ((relativep word)
           (setf words (mappend #'expand words))
           (expand-relative word words))
          ((binaryp word)
           (expand-binary word (first words) (second words)))
          (t (error "Word ~S not allowed" word))))
      (if (symbolp expression) (expand-absolute expression) (list expression))))

(defun expand-list (words &optional single)
  (multiple-value-bind (result words) (parse-list words t single)
    (values (expand result) words)))

(defun expand-text (string)
  (apply #'values (expand (parse-text string))))

(defmacro text (string)
  `(progn ,@(multiple-value-list (expand-text string))))

(defun call (expression)
  (if (consp expression)
      (apply (symbol-value (car expression)) (mapcar #'call (cdr expression)))
      (if (symbolp expression) (symbol-value expression) expression)))

(defun call-text (string)
  (let (result)
    (dolist (element (multiple-value-list (expand-text string)))
      (setf result (call element)))
    result))

(defun words (&rest words)
  (intern-all (format nil "~{~a~^ ~}" words)))

(defun add-absolute (key &optional (fn (lambda () (list key))))
  (setf (gethash (intern-all key) *words*) (cons :absolute fn)))

(defmacro defabsolute (key &optional value)
  `(progn ,@(mapcar (lambda (key) `(add-absolute ',key ,@(if value (list `(lambda () ,value)))))
                    (ensure-list key))))

(defmacro defabsolutes (&body body)
  `(progn ,@(mapcar (lambda (args) `(defabsolute ,@(if (listp args) args (list args)))) body)))

(defun expand-absolute (key)
  (funcall (cdr (gethash key *words*))))

(defun make-default-relative (key)
  (lambda (words)
    (list (cons (intern-all key) words))))

(defun add-relative (key &optional (fn (make-default-relative key)))
  (setf (gethash (intern-all key) *words*) (cons :relative fn)))

(defmacro defrelative (key &optional words &body body)
  `(progn ,@(mapcar (lambda (key) `(add-relative ',key ,@(if words (list `(lambda (,words) ,@body)))))
                    (ensure-list key))))

(defmacro defrelatives (&body body)
  `(progn ,@(mapcar (lambda (args) `(defrelative ,@(if (listp args) args (list args)))) body)))
    
(defun expand-relative (key words)
  (funcall (cdr (gethash key *words*)) words))

(defun make-default-binary (key)
  (lambda (left right)
    (list `(,key ,left ,right))))

(defun add-binary (key &optional (fn (make-default-binary key)))
  (setf (gethash (intern-all key) *words*) (cons :binary fn)))

(defun expand-binary (key left right)
  (funcall (cdr (gethash key *words*)) left right))

(defmacro defbinary (key &optional left right &body body)
  `(progn
     ,@(mapcar (lambda (key) `(add-binary ',key ,@(if left (list `(lambda (,left ,right) ,@body)))))
               (ensure-list key))))

(defmacro defbinaries (&body body)
  `(progn ,@(mapcar (lambda (args) `(defbinary ,@(if (listp args) args (list args)))) body)))

(defun make-default-modifier (key)
  (lambda (word)
    (let ((new-word (words word key)))
      (funcall
       (cond
         ((absolutep word) #'add-absolute)
         ((relativep word) #'add-relative)
         ((binaryp word) #'add-binary)
         (t (error "Modifier ~S not defined for undefined word" word)))
       new-word)
      new-word)))

(defun form (args)
  (if (listp args)
      (mapcar #'form args)
      (intern-all args)))

(defun add-modifier (key
                     &optional (fn (make-default-modifier key)))
  (setf (gethash (intern-all key) *words*) (cons :modifier fn)))

(defun expand-modifier (key word)
  (funcall (cdr (gethash key *words*)) word))

(defun get-word-kind (word)
  (if-let ((symbol (car (gethash word *words*))))
    (string-downcase (symbol-name symbol))
    "undefined"))

(defun get-definer (args)
  (with-gensyms (result)
    (destructuring-bind (keyword key &rest args) args
      `(let ((,result ,key))
         ,(case keyword
            (:absolute `(add-absolute ,result ,@(if args (list `(lambda () ,@args)))))
            (:relative `(add-relative ,result
                                      ,@(if args
                                            (list `(lambda (,(car args))
                                                     ,@(cdr args))))))
            (:binary `(add-binary ,result
                                  ,@(if args
                                        (list `(lambda (,(first args) ,(second args))
                                                 ,@(cddr args))))))
            (:default (if args (error "No arguments allwed for default definition")))
            (t (error "No keyword called ~S" keyword)))
         ,result))))

(defmacro defmodifier (key &optional word &key absolute relative binary)
  `(progn
     ,@(mapcar
        (lambda (key)
          `(add-modifier ,key
                         ,@(if word
                               (list
                                `(lambda (,word)
                                   (cond
                                     ,@(if absolute
                                           (list `((absolutep ,word)
                                                   ,(get-definer absolute))))
                                     ,@(if relative
                                           (list `((relativep ,word)
                                                   ,(get-definer relative))))
                                     ,@(if binary
                                           (list `((binaryp ,word)
                                                   ,(get-definer binary))))
                                     (t (error "Modifier ~S not defined for ~a words"
                                                ,word (get-word-kind ,word)))))))))
        (ensure-list key))))

(defmacro defmodifiers (&body body)
  `(progn ,@(mapcar (lambda (args) `(defmodifier ,@(if (listp args) args (list args)))) body)))

(defun make-default-expander (key)
  (lambda (words)
    (let ((word (words key (pop words))))
      (add-absolute word)
      (values (list word) words))))

(defun add-expander (key
                     &optional (fn (make-default-expander key)))
  (setf (gethash (intern-all key) *words*) (cons :expander fn)))

(defun expand-expander (key words)
  (funcall (cdr (gethash key *words*)) words))

(defmacro defexpander (key &optional words &body body)
  `(progn ,@(mapcar (lambda (key) `(add-expander ',key ,@(if words (list `(lambda (,words) ,@body)))))
                    (ensure-list key))))

(defmacro defexpanders (&body body)
  `(progn ,@(mapcar (lambda (args) `(defexpander ,@(if (listp args) args (list args)))) body)))
    




(defun file-string (file)
  (with-open-file (stream file :if-does-not-exist :create)
    (coerce 
     (loop for char = (read-char stream nil)
        while char
        collect char)
     'string)))

(defun parse-file (file)
  (parse-text (file-string file)))

(defun expand-file (file)
  (expand-text (file-string file)))

(defun call-file (file)
  (call-text (file-string file)))

(defmacro file (file)
  `(text ,(file-string file)))

(defmacro defcall (name lambda-list &body body)
  `(setf (symbol-value ',(intern-all name))
         (lambda ,lambda-list ,@body)))

(defmacro with-context (&body body)
  `(let ((*words* (make-hash-table)))
     (defrelative "" words words)
     ,@body))

(defun make-documentation (file)
  (with-open-file (stream file :if-does-not-exist :create :if-exists :supersede :direction :output)
    (let (absolute relative binary modifiers expanders)
      (maphash (lambda (key value)
                 (case (car value)
                   (:absolute (push key absolute))
                   (:relative (push (format nil "~a 'A" key) relative))
                   (:binary (push (format nil "'A ~a 'B" key) binary))
                   (:modifier (push (format nil "_ ~a" key) modifiers))
                   (:expander (push (format nil "~a …" key) expanders))))
               *words*)
      (let ((fn (lambda (name) (format stream "* `~a`:~%" name))))
        (loop for header in '("Absolute words"
                              "Relative words"
                              "Right associative, binary words"
                              "Modifiers"
                              "Expanders")
           for words in (list absolute relative binary modifiers expanders)
           do (format stream "# ~a~%~%" header)
           do (dolist (word (stable-sort (sort words #'string-lessp) #'< :key #'length))
                (funcall fn word))
           do (format stream "~%~%"))))))

(defmacro with-default-words (&body body)
  `(with-context
     (let ((id 0)
           (table (make-hash-table :test #'equal)))
       (defabsolutes
         ("o" nil) ("no" (expand `("not")))
         "i" "u" "ju" "vo" "jo" "jov" ;personal pronouns
         "nav" "tav" "ten" ;time
         "saj" "taj" "faj" "noj" "pju" ;attributes
         "ri" "ra" "ro" ;relative directions
         "zi" "un" ;numbers
         "coj" "zit" "sran" ;objects
         "pre" "vif" "cov" ;actions
         "it" ;relative pronouns
         "ti" ;demonstrative pronouns
         "vil"
         )

         (defrelatives
           ("re" words words)
           ("not" words
                  (if (and (not (cdr words)) (consp (car words)) (equal (caar words) "not"))
                      (cdar words)
                      (list `("not" ,@words))))
           ("a" words
                (let ((id (incf id)))
                  (dolist (word words)
                    (setf (gethash word table) id))
                  (let ((name (words "te" id)))
                    (add-relative name)
                    (list `(,name ,@words)))))
           ("te" words
                 (let ((id (or (some (lambda (word) (gethash word table)) words)
                               (error "Not talked about this object yet"))))
                   (let ((name (words "te" id)))
                     (list `(,name ,@words)))))
           "te" "a"
           "ca" "ci" "cu" "uc" "in"
           "can"
           "sun" "nur"
           "tu"
           )

       (defbinaries
         ("is" left right
               (expand `("not" ,left ("not" ,right))))
         ("si" left right
               (expand `("not" ("not" ,left) ,right)))
         ("en" left right
               (expand `("not" ("not" ,left) ("not" ,right))))
         ("ic" left right
               (expand `("" ("is" ,left ,right) ("si" ,left ,right))))
         ("or" left right
               (expand `("not" ("ic" ,left ,right)))))

       (defmodifiers
         "on" "xu" "xvon" "fi" "fjon" "fix" "xjon" "voj" "xi" ;octal number modifiers

         "nex" ;negate
         "puj" ;dot

         ;;adjective modifiers
         ("po" word ;get value of objects
               :absolute (:relative (words word "po")))
         ("poj" word ;generate set of objects having a value
               :absolute (:relative (words word "poj")))

         ;;ordered adjective modifiers
         ("pi" word
               :absolute (:relative (words word "pi")))
         ("pa" word
               :absolute (:relative (words word "pa")))
         ("pix" word
                :absolute (:relative (words word "pix")))
         ("pax" word
                :absolute (:relative (words word "pax")))
         ("pif" word
                :absolute (:relative (words word "pif")))
         ("paf" word
                :absolute (:relative (words word "paf")))
         ("piz" word
                :absolute (:relative (words word "piz")))
         ("paz" word
                :absolute (:relative (words word "paz")))

         ("nan" word
                :absolute (:relative (words word "nan")))

         ("ran" word
                :absolute (:relative (words word "ran")))

         ("su" word
               :relative (:relative (words word "su")))

         ("nen" word
                :absolute (:absolute (words word "nen")
                                     (expand `("not" ,word)))
                :relative (:relative (words word "nen") words (expand `("not" (,word ,@words))))
                :binary (:binary (words word "nen")
                                 left right
                                 (expand `("not" (,word ,left ,right))))))
       (defexpanders "n" "ov" "pin" "tez"
                     ("zu" words
                           (multiple-value-bind (result words) (expand-list words t)
                               (values result words))))
       ,@(or body (list '*words*)))))
