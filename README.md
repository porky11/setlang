This language aims to be a speakable language.
It's based on symbolic expressions, but in order to remain speakable it gets rid of explicite brackets.
In order to do this, the language has multiple kinds of words.

# Words

There are five kinds of words:
* absolute
* relative
* binary
* modifiers
* expanders

All these words have different grammatical purposes and logical meanings.

## Absolute words

Absolute words represent a single set of objects.
A set can also be ordered.

## Relative words

Relative words represent a function, which maps one set of objects to another one.

## Rightassociative binary words

These are similar to relative words, but they accept two sets as arguments and are written between those. Both arguments may have different meanings.

## Modifiers

A modifier is written after another word. It changes the meaning of the previous word.
Modifiers change the meaning of existing word and may also change the kind.
In order to simplify the language, a modifier can only expand the previous word to absolute, relative or rightassociative binary words.
If the kind of the prevoius word is a set and expands to a new word, you may think, binary words are not needed anymore, and they really work very similar in the most common cases. But in this case, the left side can only be a single word, with binary words, the left side can be a expression containing relative sets, so the associativity of the left side is different.

## Expanders

Expanders are the most powerful words. One expander is allowed to change the whole grammar of the following words of a sentence.
If an expander appers in a sentence, then the next word is not interpreted as usual.
They can define new words of any kind. They are not allowed to access information of defined words, except implicitely by expanding partial sentences with regular syntax.
